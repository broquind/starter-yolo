import boot from './boot'

export const plugin = {
  install(Vue, options) {},
}

export default (options = {}) => {
  boot(options)
}
