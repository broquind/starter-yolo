import path from 'path'
import Vue from 'vue'

function optionMerge(options) {
  return {
    mount: 'app',
    axios: {
      credentials: true,
    },
    ...options,
  }
}

export default (options) => {
  options = optionMerge(options)

  new Vue().$mount(`#${mount}`)
}

console.log(path.resolve('.'))
